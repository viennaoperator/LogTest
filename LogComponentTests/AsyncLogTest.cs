﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LogComponent;
using System.IO;
using System;
using System.Threading;

namespace LogComponentTests
{
    [TestClass]
    public class AsyncLogTest
    {
        /// <summary>
        /// Tests if a call to iLog will end up in writing something
        /// </summary>
        [TestMethod]
        public void TestWriteSomethingWithILog()
        {
            string logMessage = "This is a test message";

            ILog logger = new AsyncLogFixed();
            logger.Info(logMessage);
            logger.StopWithFlush();

            //Assumption Comment: In this unit test I just check, if the methods of the interface can be executed without exceptions
            //the unit test "TestSoftStop" is checking, if the logMessage is actually written to the file

            //note: I'm not deleting the created file here on purpose
        }

        /// <summary>
        /// Tests, if new a new log file is created when midnight is crossed
        /// </summary>
        [TestMethod]
        public void TestNewLogFileAfterMidnight(){
            AsyncLogFixed logger = new AsyncLogFixed();

            //write message before midnight
            logger.Info("This is the test message before midnight");
            //wait until message is written to file
            Thread.Sleep(logger.RunEvery * 2);
            String FilePathBeforeMidnight = logger.GetCompleteFilePath();

            //then we test if a new file is created when after midnight
            //set date after midnight
            DateTime tomorrow = DateTime.Today.AddHours(24);
            logger.CurDate = tomorrow;

            //wirte message after midnight
            logger.Info("This is the test message after midnight");
            //wait until message is written
            Thread.Sleep(logger.RunEvery * 2);
            String FilePathAfterMidnight = logger.GetCompleteFilePath();

            //check whether new file has been created
            Assert.AreNotEqual(FilePathBeforeMidnight, FilePathAfterMidnight);
            Assert.IsTrue(File.Exists(FilePathBeforeMidnight));
            Assert.IsTrue(File.Exists(FilePathAfterMidnight));

            logger.StopWithoutFlush();
            //clean up directory
            Thread.Sleep(logger.RunEvery * 2);
            File.Delete(FilePathBeforeMidnight);
            File.Delete(FilePathAfterMidnight);
        }

        /// <summary>
        /// Stops the logging immediately and tests, if the Logging is also stopped
        /// </summary>
        [TestMethod]
        public void TestForceStop()
        {
            //declaration
            String logMessage = "Random Log Message";

            //write to file
            AsyncLogFixed logger = new AsyncLogFixed();
            logger.Info(logMessage);

            //stop immediately
            logger.StopWithoutFlush();

            Thread.Sleep(logger.RunEvery * 2);

            //read content & check if it was inserted
            string contents = File.ReadAllText(logger.GetCompleteFilePath());
            Assert.IsFalse(contents.Contains(logMessage));

            //cleanup
            File.Delete(logger.GetCompleteFilePath());
        }


        /// <summary>
        /// Stops the Logging and checks, if Logs will finish after the stop
        /// </summary>
        [TestMethod]
        public void TestSoftStop()
        {
            //declaration
            String logMessage = "Random Log Message";

            //write to file
            AsyncLogFixed logger = new AsyncLogFixed();
            logger.Info(logMessage);

            //stop
            logger.StopWithFlush();

            Thread.Sleep(logger.RunEvery * 2);

            //read content & check if text was actually written to log after stopping
            string contents = File.ReadAllText(logger.GetCompleteFilePath());
            Assert.IsTrue(contents.Contains(logMessage));

            //cleanup
            File.Delete(logger.GetCompleteFilePath());
        }

    }
}
