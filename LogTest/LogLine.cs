﻿using System;
using System.Text;

//Refactor comment: cleanup methods, variables
namespace LogComponent
{

    /// <summary>
    /// This is the object that the diff. loggers (filelogger, consolelogger etc.) will operate on. The LineText() method will be called to get the text (formatted) to log
    /// </summary>
    public class LogLine
    {
   
        #region Properties

        /// <summary>
        /// The text to be display in logline
        /// </summary>
        public string Text { get; set; } = "";
        public virtual DateTime Timestamp { get; set; }

        #endregion

        #region Public Methods

        /// <returns>formatted line</returns>
        public virtual string LineText()
        {
            StringBuilder sb = new StringBuilder();

            if (this.Text.Length > 0)
            {
                sb.Append(Timestamp.ToString("yyyy-MM-dd HH:mm:ss:fff"));
                sb.Append("\t" + this.Text + ".");
                sb.Append(Environment.NewLine);
            }

            return sb.ToString();
        }

        #endregion

    }
}