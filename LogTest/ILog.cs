﻿//Refactoring comment: removed unnecessary imports
namespace LogComponent
{
    public interface ILog
    {
        /// <summary>
        /// Stop the logging. If there are any outstanding logs theses will not be written to Log
        /// </summary>
        void StopWithoutFlush();

        /// <summary>
        /// Stop the logging. All existing logs will be written to file, new logs will be discared.
        /// </summary>
        void StopWithFlush();

        //Refactoring comment: Logs usally have different methods to indicate the importance of 
        //the log and probably format the log in da different way

        /// <summary>
        /// Writes a message to the Log.
        /// </summary>
        /// <param name="text">The text that should be written to the log</param>
        void Info(string text);

        /// <summary>
        /// Writes a debug message to the Log.
        /// </summary>
        /// <param name="text">The text that should be written to the log</param>
        void Debug(string text);

        /// <summary>
        /// Writes a warning message to the Log.
        /// </summary>
        /// <param name="text">The text that should be written to the log</param>
        void Warn(string text);

        /// <summary>
        /// Write an error message to the Log.
        /// </summary>
        /// <param name="text">The text that should be written to the log</param>
        void Error(string text);

        /// <summary>
        /// Writes the Traceback to the log
        /// </summary>
        /// <param name="text">The text that should be written to the log</param>
        void Trace(string text);
    }
}
