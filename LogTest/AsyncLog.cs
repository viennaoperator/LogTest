﻿//Refacotring comment: I reorganized imports outside of namespace
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

//Refacotring comment: rename misleading namespace
namespace LogComponent
{
    public class AsyncLogFixed : ILog
    {
        //Refacotring comment: group code into regions like LogLine.cs

        #region Private Fields
        //Refacotring comment: reorganized variables due to readability
        //Refactoring comment: using a q data structure in order to get fire and forget message(log) queuing.
        private Queue<LogLine> _lines;
        private StreamWriter _writer;
        //Refacotring comment: private field due to StopWithFlush()
        private bool _quitWithFlush = false;
        //Refactoring comment: using timer instead of threads --> saving resources, timer reuses threads
        private Timer _timerMainLoop;
        private Timer _timerCheckMidnight;
        #endregion

        #region Properties

        //Refactor comment: We could put the properties into an external .properties file
        //preferences of asynclog
        public DateTime CurDate { get; set; } = DateTime.Now;
        public String DirectoryPath { get; set; } = @"C:\LogTest";
        public String FileName { get; set; }

        /// <summary>
        /// defines the intervall the async log should run in milliseconds
        /// </summary>
        public int RunEvery { get; set; } = 50;
        #endregion

        #region Constructors
        public AsyncLogFixed()
        {

            if (!Directory.Exists(DirectoryPath))
            {
                Directory.CreateDirectory(DirectoryPath);
            }

            this._lines = new Queue<LogLine>();

            this.GenerateNewFile();

            //define interval of timer(s)
            var startTimeSpan = TimeSpan.Zero;
            var periodTimeSpan = TimeSpan.FromMilliseconds(this.RunEvery);

            _timerMainLoop = new Timer((e) =>
            {
                this.MainLoop();
            }, null, startTimeSpan, periodTimeSpan);

            //Refactoring comment: I defined the same timer for the midnight check and main loop
            //but now we could define different intervals for both requirements, for example:
            //Check for new messages every 50 Milliseconds and check if log should create new log 
            //file every minute
            _timerCheckMidnight = new Timer((e) =>
            {
                this.CheckMidnight();
            }, null, startTimeSpan, periodTimeSpan);

        }
        #endregion Constructors

        #region Public Methods

        /// <summary>
        /// stop the logging immediately
        /// </summary>
        public void StopWithoutFlush()
        {
            this._timerMainLoop.Dispose();
            this._writer.Close();
        }

        /// <summary>
        /// locks the message queue and writes
        /// the left messages to the log
        /// </summary>
        public void StopWithFlush()
        {
            this._quitWithFlush = true;
        }

        /// <summary>
        /// returns the path to the current log file
        /// </summary>
        public String GetCompleteFilePath()
        {
            return DirectoryPath + @"\" + FileName;
        }

        public void log(LogLine _logLine)
        {
            //we have to check, if we already closed the logger otherwise we could go on and push log messages
            if (!this._quitWithFlush)
            {
                this._lines.Enqueue(_logLine);
            }
            else
            {
                Console.WriteLine("Logger was closed gracefully, will not process this log message");
            }
        }

        public void Error(string text)
        {
            this.log(new LogLine() { Text = $"ERROR - {text}", Timestamp = DateTime.Now });
        }

        public void Debug(string text)
        {
            this.log(new LogLine() { Text = $"DEBUG - {text}", Timestamp = DateTime.Now });
        }
        // Refactor comment: could be enhanced with LOGLEVEL attribute in LogLine Class
        public void Info(string text)
        {
            this.log(new LogLine() { Text = $"INFO - {text}", Timestamp = DateTime.Now });
        }

        public void Warn(string text)
        {
            this.log(new LogLine() { Text = $"WARN - {text}", Timestamp = DateTime.Now });
        }

        public void Trace(string text)
        {
            this.log(new LogLine() { Text = $"TRACE - {text}", Timestamp = DateTime.Now });
        }

        #endregion Public Methods

        #region Private Methods

        private static String GenerateNewFileName()
        {
            return "Log" + DateTime.Now.ToString("yyyyMMdd HHmmss fff") + ".log";
        }

        private void GenerateNewFile()
        {
            FileName = GenerateNewFileName();
            this._writer = File.AppendText(GetCompleteFilePath());
            //Refactoring comment: Usally log files don't have headers --> uncomment
            //this._writer.Write("Timestamp".PadRight(25, ' ') + "\t" + "Data".PadRight(15, ' ') + "\t" + "\n \n");
            this._writer.AutoFlush = true;
        }

        /// <summary>
        /// Checks whether a new file needs to be created (after midnight)
        /// </summary>
        private void CheckMidnight()
        {
            //if ((DateTime.Now - CurDate).Days != 0) --> generates a log file every 24 hours instead of every new actual date
            //fix:  if (DateTime.Now.DayOfYear != CurDate.DayOfYear)
            if (DateTime.Now.DayOfYear != CurDate.DayOfYear)
            {
                //we need to close the file here
                this._writer.Close();
                CurDate = DateTime.Now;
                this.GenerateNewFile();
            }
        }

        /// <summary>
        /// This is the main loop of the async logger
        /// it checks for new messages and locks the
        /// log if the close method is called
        /// </summary>
        private void MainLoop()
        {
            //Refactor comment: int f not needed
            //Refactor comment: handled not needed
            this.writeAllToFile();
            if (this._quitWithFlush == true && this._lines.Count == 0)
            {
                this._writer.Close();
                this._timerMainLoop.Dispose();
            }
        }

        /// <summary>
        /// Checks whether new messages have been added 
        /// to the log queue and writes them to a log file.
        /// </summary>
        private void writeAllToFile()
        {
            while (this._lines.Count > 0)
            {
                //Refactor comment: StringBuilder will be initialized in LogLine, less initialization, less memory used --> better performance
                //Refactor comment: Implementing requirement that application should continue running if errors occurs
                try
                {
                    LogLine logLineToWrite = this._lines.Dequeue();
                    this._writer.Write(logLineToWrite.LineText());
                }
                catch (Exception e)
                {
                    Console.WriteLine("Log file is already locked....");
                }
            }
        }

        #endregion Private Methods
    }
}