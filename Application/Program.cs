﻿//Refactor comment: Reorganized imports
using System;
using LogComponent;

namespace LogUsers
{

    class Program
    {
        /// <summary>
        /// writes two log files Two log-files, one with numbers going from 50 down
        /// to something – when the component is stopped without flush. 
        /// One file having logs with numbers going from 0 to 14
        /// </summary>
        static void Main(string[] args)
        {
            ILog logger = new AsyncLogFixed();

            for (int i = 0; i < 15; i++)
            {
                logger.Info("Number with Flush: " + i.ToString());
            }

            logger.StopWithFlush();

            //Refactoring comment: You might want to increase the number to check if the lock 
            //of the log is working properly
            ILog logger2 = new AsyncLogFixed();

            for (int i = 50; i > 0; i--)
            {
                logger2.Info("Number with No flush: " + i.ToString());
            }

            logger2.StopWithoutFlush();

            Console.ReadLine();
        }
    }
}
